package com.wsx.ones.hello.msg.service;

import com.wsx.ones.hello.msg.model.Msg;

/**
 * Created by wangshuaixin on 16/12/10.
 */
public interface MsgService {

    boolean saveMsg(Msg msg);
}
