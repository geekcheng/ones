package com.wsx.ones.hello;


import com.wsx.ones.web.controller.BaseController;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.BaseUser;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangshuaixin on 16/12/10.
 */
@RestController
@RequestMapping("/Hello")
public abstract class HelloBaseController extends BaseController implements WebController {

    protected boolean checkMsg(HelloBaseBean bean) {
        if (null == bean) {
            return false;
        }
        return true;
    }

    @Override
    public BaseUser setUserCache(BaseUser baseUser, boolean ifEhcache) {
        return null;
    }

    @Override
    public BaseUser getUserCache(String uid, boolean ifEhcache) {
        return null;
    }
}
