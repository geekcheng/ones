package com.wsx.ones.hello.msg.service.impl;

import com.wsx.ones.hello.msg.dao.MsgDao;
import com.wsx.ones.hello.msg.model.Msg;
import com.wsx.ones.hello.msg.service.MsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wangshuaixin on 16/12/10.
 */
@Service
@Transactional
public class MsgServiceImpl implements MsgService {

    @Autowired
    private MsgDao msgDao;

    @Transactional
    public boolean saveMsg(Msg msg) {
        return false;
    }
}
