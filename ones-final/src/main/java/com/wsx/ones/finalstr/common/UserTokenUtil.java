package com.wsx.ones.finalstr.common;

/**
 * Created by wangshuaixin on 16/12/19.
 */
public final class UserTokenUtil {

    public static final String COOKIE_USER_TOKEN = "ONESPID";

    public static final String COOKIE_USER_PATH = "/";

    public static final String COOKIE_USER_DOMAIN = "ones.com";

    public static final int COOKIE_USER_AGE = 24 * 60 * 60;
}
