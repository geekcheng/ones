package com.wsx.ones.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期工具
 */
public class DateUtil {

	public static final String YYYYMMDDHHMMSS = "yyyy-MM-dd hh:mm:ss";
	public static final String YYYYMMDD = "yyyy-MM-dd";
	
	public static String getNowDate() {
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat(YYYYMMDDHHMMSS);
		return dateFormat.format(date);
	}

	public static Date getDate() {
		return new Date();
	}

	public static String getStringDate(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(YYYYMMDDHHMMSS);
		return dateFormat.format(date);
	}

	public static Date getDateFromStr(String dateStr) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(YYYYMMDD);
		Date date = null;
		try {
			date = dateFormat.parse(dateStr);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}
}
