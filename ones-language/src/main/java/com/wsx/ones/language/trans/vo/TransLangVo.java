package com.wsx.ones.language.trans.vo;

import com.wsx.ones.web.swap.ReturnStatus;
import com.wsx.ones.web.vo.BaseVo;

import java.util.Map;

/**
 * Created by wangshuaixin on 17/1/5.
 */
public class TransLangVo extends BaseVo {

    private static final long serialVersionUID = 7235821480614651208L;

    public TransLangVo () {
        super();
    }
    public TransLangVo(ReturnStatus returnStatus) {
        super(returnStatus);
    }

    private Map<String, Object> result;

    public Map<String, Object> getResult() {
        return result;
    }

    public void setResult(Map<String, Object> result) {
        this.result = result;
    }
}
