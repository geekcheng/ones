package com.wsx.ones.language;

import com.wsx.ones.web.model.OutputData;

import java.util.Map;

/**
 * Created by wangshuaixin on 16/12/28.
 */
public class LanguageData extends OutputData{

    private static final long serialVersionUID = -5444329593692705865L;

    public LanguageData () {
        super();
    }
    public LanguageData(int code, int status) {
        super(code, status);
    }

    private Map<String, Object> result;

    public Map<String, Object> getResult() {
        return result;
    }

    public void setResult(Map<String, Object> result) {
        this.result = result;
    }
}
