package com.wsx.ones.pay.recharge.dao.impl;

import com.wsx.ones.pay.recharge.dao.RechargeDao;
import com.wsx.ones.pay.recharge.mapper.RechargeMapper;
import com.wsx.ones.pay.recharge.model.Recharge;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * Created by wangshuaixin on 16/12/8.
 */
@Repository
public class RechargeDaoImpl implements RechargeDao {

    @Resource
    private RechargeMapper rechargeMapper;

    public boolean saveRecharge(Recharge recharge) {

        return rechargeMapper.saveRecharge(recharge);
    }
}
