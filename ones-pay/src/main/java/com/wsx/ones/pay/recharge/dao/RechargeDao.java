package com.wsx.ones.pay.recharge.dao;

import com.wsx.ones.pay.recharge.model.Recharge;

/**
 * Created by wangshuaixin on 16/12/8.
 */
public interface RechargeDao {

    boolean saveRecharge(Recharge recharge);
}
