package com.wsx.ones.pay.recharge.model;

import com.wsx.ones.core.model.BaseBean;

/**
 * Created by wangshuaixin on 16/12/8.
 */
public class Recharge extends BaseBean {

    private static final long serialVersionUID = 7271040265778692623L;

    private String test;

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }
}
