package com.wsx.ones.pay.recharge;

import com.wsx.ones.pay.PayBaseController;
import com.wsx.ones.pay.recharge.model.Recharge;
import com.wsx.ones.pay.recharge.service.RechargeService;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.OutputData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by wangshuaixin on 16/12/8.
 */
@RestController
@Scope("prototype")
public class RechargeController extends PayBaseController {

    @Autowired
    private RechargeService rechargeService;


    public OutputData saveRecharge(Recharge recharge, HttpServletRequest request, HttpServletResponse response) {
        if (!checkData()) {
            return new OutputData(WebController.CODE_SUCCESS, WebController.STATUS_ERROR_CHECKCODE);
        }

        boolean isSave = rechargeService.saveRecharge(recharge);

        if (!isSave) {
            return new OutputData(WebController.CODE_SUCCESS, WebController.STATUS_ERROR_CHECKCODE);
        }

        return new OutputData();
    }
}
