package com.wsx.ones.pay.account;

import com.wsx.ones.pay.PayBaseController;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangshuaixin on 16/12/14.
 */
@RestController
@Scope("prototype")
public class PayAccountController extends PayBaseController {


}
