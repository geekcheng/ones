package com.wsx.ones.pay.recharge.mapper;

import com.wsx.ones.pay.recharge.model.Recharge;

/**
 * Created by wangshuaixin on 16/12/8.
 */
public interface RechargeMapper {

    public boolean saveRecharge(Recharge recharge);

}
