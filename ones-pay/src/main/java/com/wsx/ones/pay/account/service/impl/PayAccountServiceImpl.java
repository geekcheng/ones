package com.wsx.ones.pay.account.service.impl;

import com.wsx.ones.pay.account.service.PayAccountService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wangshuaixin on 16/12/14.
 */
@Service
@Transactional
public class PayAccountServiceImpl implements PayAccountService {
}
