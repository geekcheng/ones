package com.wsx.ones.pay.recharge.service;

import com.wsx.ones.pay.recharge.model.Recharge;

/**
 * Created by wangshuaixin on 16/12/8.
 */
public interface RechargeService {

    boolean saveRecharge(Recharge recharge);
}
