package com.wsx.ones.web.model;

import com.wsx.ones.web.BoVo;

import java.util.List;


/**
 * 主要设计处理接口传入参数，controller层接收的参数类型
 * Created by wangshuaixin on 16/12/16.
 */
public class InputParam implements BoVo {

    private static final long serialVersionUID = -6480879382238336818L;

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
