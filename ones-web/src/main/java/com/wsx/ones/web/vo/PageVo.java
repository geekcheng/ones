package com.wsx.ones.web.vo;

import com.wsx.ones.web.swap.ReturnStatus;

import java.util.List;
import java.util.Map;

/**
 * Created by wangshuaixin on 16/12/29.
 */
public class PageVo extends BaseVo {

    private static final long serialVersionUID = -3257833277421891128L;

    public PageVo() {
        super();
    }
    public PageVo(ReturnStatus returnStatus) {
        super(returnStatus);
    }

    public PageVo(ReturnStatus returnStatus, List<Map<String, String>> results) {
        super(returnStatus);
        this.results = results;
    }

    private List<Map<String, String>> results;

    public List<Map<String, String>> getResults() {
        return results;
    }

    public void setResults(List<Map<String, String>> results) {
        this.results = results;
    }

    private List<Map<String, Object>> rtnObjs;

    public List<Map<String, Object>> getRtnObjs() {
        return rtnObjs;
    }

    public void setRtnObjs(List<Map<String, Object>> rtnObjs) {
        this.rtnObjs = rtnObjs;
    }
}
