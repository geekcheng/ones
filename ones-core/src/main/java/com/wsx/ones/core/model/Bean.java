package com.wsx.ones.core.model;

import java.io.Serializable;

/**
 * 所有bean的超类
 * Created by wangshuaixin on 16/12/23.
 */
public interface Bean extends Serializable {

}
