package com.wsx.ones.settings.system.service.impl;

import com.wsx.ones.settings.system.service.SystemService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wangshuaixin on 16/12/11.
 */
@Service
@Transactional
public class SystemServiceImpl implements SystemService {
}
