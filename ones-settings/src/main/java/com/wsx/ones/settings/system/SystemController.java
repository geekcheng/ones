package com.wsx.ones.settings.system;

import com.wsx.ones.settings.SettingsBaseController;
import com.wsx.ones.settings.system.service.SystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangshuaixin on 16/12/11.
 */
@RestController
@Scope("prototype")
public class SystemController extends SettingsBaseController {

    @Autowired
    private SystemService systemService;
}
