package com.wsx.ones.ad;

import com.wsx.ones.web.controller.BaseController;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.BaseUser;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/Ad")
public abstract class AdBaseController extends BaseController implements WebController {

    /**
     *
     * @param adParam
     * @return
     */
    protected boolean checkAd(AdParam adParam) {
        if (null == adParam) {
            return false;
        }
        return true;
    }

    @Override
    public BaseUser setUserCache(BaseUser baseUser, boolean ifEhcache) {
        return null;
    }

    @Override
    public BaseUser getUserCache(String uid, boolean ifEhcache) {
        return null;
    }
}
