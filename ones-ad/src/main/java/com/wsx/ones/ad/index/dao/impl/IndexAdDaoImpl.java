package com.wsx.ones.ad.index.dao.impl;

import com.wsx.ones.ad.index.dao.IndexAdDao;
import com.wsx.ones.ad.index.mapper.IndexAdMapper;
import com.wsx.ones.ad.index.model.Ad;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * Created by wangshuaixin on 16/12/9.
 */
@Repository
public class IndexAdDaoImpl implements IndexAdDao {

    @Resource
    private IndexAdMapper indexAdMapper;

    public boolean saveAd(Ad ad) {
        int num = indexAdMapper.saveAd(ad);
        if (num <= 0) {
            return false;
        }
        return true;
    }

    @Override
    public List<Map<String, Object>> getPageHeaderAd() {
        return indexAdMapper.getPageHeaderAd();
    }
}
