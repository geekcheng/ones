package com.wsx.ones.search;

import com.wsx.ones.web.controller.BaseController;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.BaseUser;
import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("Es")
public abstract class EsBaseController extends BaseController implements WebController {

    protected boolean checkSearchInput(SearchParam searchParam) {
        if (null == searchParam) {
            return false;
        }
        if (StringUtils.isEmpty(searchParam.getToken())) {
            return false;
        }
        if (StringUtils.isEmpty(searchParam.getName())) {
            return false;
        }
        return true;
    }

    @Override
    public BaseUser setUserCache(BaseUser baseUser, boolean ifEhcache) {
        return null;
    }

    @Override
    public BaseUser getUserCache(String uid, boolean ifEhcache) {
        return null;
    }
}
