package com.wsx.ones.search.es.goods;


import com.wsx.ones.search.EsBaseController;
import com.wsx.ones.search.SearchBaseBean;
import com.wsx.ones.search.SearchParam;
import com.wsx.ones.search.es.EsOutputDatas;
import com.wsx.ones.search.es.goods.service.GoodsSearchService;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.OutputData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@Scope("prototype")
public class GoodsSearchController extends EsBaseController {

    @Autowired
    private GoodsSearchService goodsSearchService;


    @RequestMapping(
            value = "/search/goods",
            method = {RequestMethod.POST}
    )
    public OutputData searchGoods(SearchParam searchParam) {

        if (!checkSearchInput(searchParam)) {
            return new OutputData(WebController.CODE_SUCCESS, WebController.STATUS_ERROR_CHECKCODE);
        }

        if (!checkToken(searchParam.getToken())) {
            return new OutputData(WebController.CODE_SUCCESS, WebController.STATUS_ERROR_CHECKCODE);
        }

        List<Map<String, Object>> list = goodsSearchService.searchGoodsByName(searchParam);
        EsOutputDatas datas = new EsOutputDatas();
        datas.setDatas(list);
        return datas;
    }
}
