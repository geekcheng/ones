package com.wsx.ones.search.es.user.service;

import java.util.List;
import java.util.Map;

import com.wsx.ones.search.es.user.model.User;


public interface UserSearchService {

	public User getPerson(String name);

	public List<Map<String, Object>> searchUsersByName(String userName);
}
