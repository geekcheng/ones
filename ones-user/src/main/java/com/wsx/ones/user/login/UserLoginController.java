package com.wsx.ones.user.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.BaseUser;
import com.wsx.ones.web.token.TokenUtil;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.wsx.ones.user.UserBaseController;
import com.wsx.ones.user.login.model.LoginUser;

@RestController
@Scope("prototype")
public class UserLoginController extends UserBaseController {

	@RequestMapping(
			value = "/user/login/nm",
			method = {RequestMethod.POST}
	)
	public String userLogin(LoginUser user, HttpServletRequest request, HttpServletResponse response) {
		
		//
		int cliCode = WebController.CODE_ERROR_CLIENT; //checkLogin(user);
		if (cliCode != WebController.STATUS_SUCCESS) {
			return "";
		}
		
		//
		//reconstructPasswd(user, true);

		//

		
		//
		return "";
	}
	
	@RequestMapping(
			value = "/user/login/test/{name}",
			method = {RequestMethod.GET}
	)
	public @ResponseBody BaseUser test(@PathVariable String name) {
		BaseUser user = new BaseUser();
		user.setLname("123");

		BaseUser baseUser = getUserCache(name, true);
		if (null == baseUser) {
			setUserCache(user, true);
		}
		return user;
	}


}
