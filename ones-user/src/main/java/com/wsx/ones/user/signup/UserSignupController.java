package com.wsx.ones.user.signup;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wsx.ones.common.util.DateUtil;
import com.wsx.ones.finalstr.common.RedisFinalUtil;
import com.wsx.ones.goods.rpc.inter.GoodsCommonInterface;
import com.wsx.ones.goods.rpc.model.Goods;
import com.wsx.ones.user.UserLoginEnum;
import com.wsx.ones.user.sequence.SequenceNum;
import com.wsx.ones.user.signup.bo.UserSignupBo;
import com.wsx.ones.user.signup.data.UserSignupData;
import com.wsx.ones.user.signup.model.UserDatas;
import com.wsx.ones.user.signup.param.UserSignupParam;
import com.wsx.ones.user.signup.vo.UserSignupVo;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.OutputData;
import com.wsx.ones.web.util.PojoCopy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;
import org.springframework.util.SystemPropertyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.wsx.ones.user.UserBaseController;
import com.wsx.ones.user.signup.model.User;
import com.wsx.ones.user.signup.model.WebUser;
import com.wsx.ones.user.signup.model.WebUserOutputData;
import com.wsx.ones.user.signup.service.UserSignupService;

@RestController
@Scope("prototype")
public class UserSignupController extends UserBaseController {

	private static final Logger log = LoggerFactory.getLogger(UserSignupController.class);
	
	
	@Autowired
	private UserSignupService userSignupService;

	@Autowired
	private GoodsCommonInterface goodsCommonInterface;
	

	
	@RequestMapping(
			value = "/signup/web/user",
			method = {RequestMethod.POST}
	)
	public String signupWebUser(WebUser user,HttpServletRequest request, HttpServletResponse response) {

		String hello = goodsCommonInterface.get();
		System.out.print(hello);

		Goods goods = goodsCommonInterface.take("wang");
		System.out.print(goods.getName() + "====" + goods.getPrice());
		
		boolean isSave = userSignupService.saveWebUser(user);
		if(isSave) {
			return "WEB-INF/jsp/user/signupWeb";
		} else {
			return "error";
		}
	}
	
	@RequestMapping(
			value = "/search/users/{userId}/{token}",
			method = {RequestMethod.GET}
	)
	public @ResponseBody OutputData getWebUsers(@PathVariable("userId") String userId,
			@PathVariable("token") String token) {
		
		if (StringUtils.isEmpty(userId)) {
			return new OutputData(CODE_ERROR_CLIENT, STATUS_ERROR_PARAM);
		}
		
		List<WebUser> users = userSignupService.getWebUsers(userId,token);
		
		WebUserOutputData data = new WebUserOutputData();
		data.setUsers(users);
		return data;
	}


	@RequestMapping(
			value = "/users/{token}",
			method = {RequestMethod.GET,RequestMethod.POST}
	)
	public @ResponseBody OutputData getUsers(@PathVariable String token) {

		List<User> users = userSignupService.findAllUsers();

		UserDatas userDatas = new UserDatas();
		userDatas.setUsers(users);
		return userDatas;
	}


	@RequestMapping(
			value = "/users/byNames/{name}/{token}",
			method = {RequestMethod.GET,RequestMethod.POST}
	)
	public OutputData getUsersByName(@PathVariable String name, @PathVariable String token) {
		List<User> users = null;
		if (null != token && null != name) {
			try {
				users = userSignupService.getUsersByName(name);
			} catch (Exception e) {
				System.out.print(e.getMessage());
			}
		}

		UserDatas userDatas = new UserDatas();
		userDatas.setUsers(users);
		return userDatas;
	}

	@RequestMapping(
			value = "/users/getName/{name}",
			method = {RequestMethod.GET}
	)
	public @ResponseBody OutputData getUserByName(@PathVariable String name) {

		List<User> users = userSignupService.findUsersByName(name);

		UserDatas datas = new UserDatas();
		datas.setUsers(users);
		return datas;
	}


	@RequestMapping(
			value = "/signup/base/user.web",
			method = {RequestMethod.POST}
	)
	public @ResponseBody OutputData saveUserInfo(UserSignupParam userSignupParam, HttpServletRequest request, HttpServletResponse response) {
		log.info("");

		//检验前台传递的数据参数
		if (!checkSignupParam(userSignupParam)) {
			return new OutputData(CODE_ERROR_CLIENT, STATUS_ERROR_PARAM);
		}

		//密码的处理
		reconPasswd(userSignupParam);

		//TODO cache
		if (checkExistUser(userSignupParam, UserLoginEnum.PHONE)) {
			return new OutputData(CODE_ERROR_CLIENT, STATUS_DATA_EXIT);
		}

		//数据补充
		UserSignupBo bo = PojoCopy.copy(userSignupParam, UserSignupBo.class);
		bo.setUid(SequenceNum.getInstance().getSequenceNum(RedisFinalUtil.SEQUENE_USER_NUM));
		bo.setRip("127.0.0.1");
		bo.setRapp("mac");
		bo.setRname("131");
		bo.setRtime(DateUtil.getDate());


		//业务操作
		UserSignupVo vo = userSignupService.signupUser(bo);

		//结果返回
		switch (vo.getReturnStatus()) {
			case SUCCESS:
				setUserTokenCookie(vo, response);
				break;
			case ERROR_SQL:
			case ERROR_UNKNOW:
				return new OutputData(WebController.CODE_ERROR_SERVER, WebController.STATUS_ERROR_UNKNOWN);
			case DATA_EXIST:
				return new OutputData(WebController.CODE_ERROR_SERVER, WebController.STATUS_DATA_EXIT);
			default:
				break;
		}

		//return "WEB-INF/jsp/user/userInfo";
		return PojoCopy.copy(vo, UserSignupData.class);
	}

}
