package com.wsx.ones.user.signup.service;

import java.util.List;

import com.wsx.ones.user.model.UserInfo;
import com.wsx.ones.user.signup.bo.UserSignupBo;
import com.wsx.ones.user.signup.model.User;
import com.wsx.ones.user.signup.model.WebUser;
import com.wsx.ones.user.signup.vo.UserSignupVo;

public interface UserSignupService {

	public User getUserById(String id);
	
	public boolean saveUser(User user);
	
	public List<User> findAllUsers();
	
	public List<User> findByUserIds(List<Integer> userIds);

	public boolean saveWebUser(WebUser user);

	public WebUser getWebUserById(String userId);

	public List<WebUser> getWebUsers(String userId, String token);

	public List<User> getUsersByName(String name);

	public List<User> findUsersByName(String name);

    UserSignupVo signupUser(UserSignupBo bo);
}
