package com.wsx.ones.user.model;

import com.wsx.ones.user.UserBaseBean;

/**
 * Created by wangshuaixin on 16/12/19.
 */
public class UserSec extends UserBaseBean {

    private static final long serialVersionUID = -8327898787771086751L;

    private Long sid;
    //private Long uid;
    private Integer ask;
    private String answer;
    private Integer status;

    //分库
    //private Integer utype;

    public Long getSid() {
        return sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    public Integer getAsk() {
        return ask;
    }

    public void setAsk(Integer ask) {
        this.ask = ask;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
