package com.wsx.ones.user.model;

import com.wsx.ones.user.UserBaseBean;

import java.util.Date;

/**
 * Created by wangshuaixin on 16/12/19.
 */
public class UserExt2 extends UserBaseBean {

    private static final long serialVersionUID = -1837870888723670412L;

    //private Long uid;
    private Date lltime;
    private String llip;
    private String llapp;

    //分库
    //private Integer utype;

    public Date getLltime() {
        return lltime;
    }

    public void setLltime(Date lltime) {
        this.lltime = lltime;
    }

    public String getLlip() {
        return llip;
    }

    public void setLlip(String llip) {
        this.llip = llip;
    }

    public String getLlapp() {
        return llapp;
    }

    public void setLlapp(String llapp) {
        this.llapp = llapp;
    }

}
