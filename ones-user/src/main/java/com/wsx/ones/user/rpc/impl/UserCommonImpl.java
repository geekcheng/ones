package com.wsx.ones.user.rpc.impl;

import com.google.common.base.Strings;
import com.wsx.ones.common.util.DateUtil;
import com.wsx.ones.user.rpc.model.TokenUser;
import org.springframework.stereotype.Service;

import com.wsx.ones.user.rpc.inter.UserCommonInterface;


@Service
public class UserCommonImpl implements UserCommonInterface {

	@Override
	public String get() {
		// TODO Auto-generated method stub
		return "hello";
	}

	@Override
	public TokenUser checkUser(String token) {
		if (Strings.isNullOrEmpty(token)) {
			return null;
		}
		TokenUser tokenUser = new TokenUser();
		tokenUser.setAtime(DateUtil.getDate());
		tokenUser.setName("wang");
		tokenUser.setNum(1001);
		return tokenUser;
	}


}
