package com.wsx.ones.user.signup.vo;

import com.wsx.ones.user.UserBaseVo;
import com.wsx.ones.web.swap.ReturnStatus;

/**
 * Created by wangshuaixin on 16/12/23.
 */
public class UserSignupVo extends UserBaseVo {

    private static final long serialVersionUID = -6656112215573301185L;

    public UserSignupVo() {
        super();
    }
    public UserSignupVo(ReturnStatus returnStatus) {
        super(returnStatus);
    }
}
