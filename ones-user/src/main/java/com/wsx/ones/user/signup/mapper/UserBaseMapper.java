package com.wsx.ones.user.signup.mapper;

import com.wsx.ones.user.model.UserBase;

/**
 * Created by wangshuaixin on 16/12/27.
 */
public interface UserBaseMapper {

    int saveUserBase(UserBase userBase);
}
