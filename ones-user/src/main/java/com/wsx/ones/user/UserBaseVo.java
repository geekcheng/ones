package com.wsx.ones.user;

import com.wsx.ones.web.swap.ReturnStatus;
import com.wsx.ones.web.vo.BaseVo;

/**
 * Created by wangshuaixin on 16/12/23.
 */
public class UserBaseVo extends BaseVo {

    private static final long serialVersionUID = 1340774541610457069L;

    public UserBaseVo() {
        super();
    }
    public UserBaseVo(ReturnStatus returnStatus) {
        super(returnStatus);
    }

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
