package com.wsx.ones.user.signup.model;

import com.wsx.ones.user.UserData;

import java.util.List;

/**
 * Created by wangshuaixin on 16/12/7.
 */

public class UserDatas extends UserData {

    private  static  final long serialVersionUID = -1L;

    public UserDatas() {
        super();
    }

    public UserDatas(int code, int status) {
        super(code, status);
    }

    private List<User> users;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
