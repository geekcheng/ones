package com.wsx.ones.custom.center.service.impl;

import com.wsx.ones.custom.center.bo.CenterBo;
import com.wsx.ones.custom.center.dao.CenterDao;
import com.wsx.ones.custom.center.model.CustomNotice;
import com.wsx.ones.custom.center.service.CenterService;
import com.wsx.ones.custom.center.vo.CenterVo;
import com.wsx.ones.web.swap.ReturnStatus;
import com.wsx.ones.web.util.PojoCopy;
import com.wsx.ones.web.vo.PageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created by wangshuaixin on 17/1/4.
 */
@Service
@Transactional
public class CenterServiceImpl implements CenterService {

    @Autowired
    private CenterDao centerDao;


    @Transactional(propagation = Propagation.REQUIRED)
    public CenterVo saveNotice(CenterBo bo) {

        CustomNotice notice = PojoCopy.copy(bo, CustomNotice.class);

        int count = centerDao.saveNotice(notice);
        if (count <= 0) {
            return new CenterVo(ReturnStatus.ERROR_UNKNOW);
        }

        CenterVo vo = new CenterVo();
        vo.setId(bo.getNid());
        return vo;
    }

    @Override
    public PageVo getFiveNotices() {

        List<Map<String, Object>> list = centerDao.getFiveNotices();
        if (null == list) {
            return new PageVo(ReturnStatus.ERROR_SQL);
        }
        if (list.size() <= 0) {
            return new PageVo(ReturnStatus.NOT_EXIST);
        }

        PageVo pageVo = new PageVo();
        pageVo.setRtnObjs(list);
        return pageVo;
    }
}
