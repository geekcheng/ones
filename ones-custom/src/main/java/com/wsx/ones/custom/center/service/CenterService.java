package com.wsx.ones.custom.center.service;

import com.wsx.ones.custom.center.bo.CenterBo;
import com.wsx.ones.custom.center.vo.CenterVo;
import com.wsx.ones.web.vo.PageVo;

/**
 * Created by wangshuaixin on 17/1/4.
 */
public interface CenterService {

    CenterVo saveNotice(CenterBo bo);

    PageVo getFiveNotices();
}
