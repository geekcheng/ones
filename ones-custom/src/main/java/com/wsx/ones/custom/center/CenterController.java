package com.wsx.ones.custom.center;

import com.wsx.ones.common.util.DateUtil;
import com.wsx.ones.custom.CustomBaseController;
import com.wsx.ones.custom.CustomData;
import com.wsx.ones.custom.center.bo.CenterBo;
import com.wsx.ones.custom.center.param.CenterParam;
import com.wsx.ones.custom.center.service.CenterService;
import com.wsx.ones.custom.center.vo.CenterVo;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.OutputData;
import com.wsx.ones.web.model.PageData;
import com.wsx.ones.web.util.OperationEnum;
import com.wsx.ones.web.util.PojoCopy;
import com.wsx.ones.web.vo.PageVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.*;

/**
 * Created by wangshuaixin on 16/12/18.
 */
@RestController
@Scope("prototype")
public class CenterController extends CustomBaseController {

    private static final Logger log = LoggerFactory.getLogger(CenterController.class);

    @Autowired
    private CenterService centerService;


    @RequestMapping(
            value = "/center/notice",
            method = {RequestMethod.POST}
    )
    public @ResponseBody OutputData saveNotice(CenterParam centerParam) {

        log.info("enter center notice");

        if (!checkCenterParam(centerParam, OperationEnum.ADD)) {
            return new OutputData(WebController.CODE_ERROR_CLIENT, WebController.STATUS_ERROR_PARAM);
        }

        CenterBo bo = PojoCopy.copy(centerParam, CenterBo.class);
        bo.setAtime(DateUtil.getDate());

        CenterVo vo = centerService.saveNotice(bo);
        switch (vo.getReturnStatus()) {
            case SUCCESS:
                break;
            case ERROR_SQL:
            case ERROR_UNKNOW:
                return new OutputData(WebController.CODE_ERROR_SERVER, WebController.STATUS_ERROR_UNKNOWN);
            default:
                break;
        }

        return PojoCopy.copy(vo, CustomData.class);
    }

    @RequestMapping(
            value = "/center/notices/{token}",
            method = {RequestMethod.GET}
    )
    public @ResponseBody OutputData getNotices(@PathVariable String token) {

        if (!checkToken(token)) {
            return new OutputData(WebController.CODE_ERROR_CLIENT, WebController.STATUS_TOKEN_ERROR);
        }

        PageVo pageVo = centerService.getFiveNotices();
        switch (pageVo.getReturnStatus()) {
            case SUCCESS:
                break;
            case ERROR_SQL:
            case ERROR_UNKNOW:
                return new OutputData(WebController.CODE_ERROR_SERVER, WebController.STATUS_ERROR_UNKNOWN);
            case NOT_EXIST:
                return new OutputData(WebController.CODE_ERROR_SERVER, WebController.STATUS_DATA_NOEXIT);
            default:
                break;
        }

        return PojoCopy.copy(pageVo, PageData.class);
    }

}
