package com.wsx.ones.custom.center.bo;

import com.wsx.ones.web.bo.BaseBo;

import java.util.Date;

/**
 * Created by wangshuaixin on 17/1/4.
 */
public class CenterBo implements BaseBo {

    private static final long serialVersionUID = 5635063409893046305L;

    private Long nid;
    private String name;
    private Date atime;

    public Long getNid() {
        return nid;
    }

    public void setNid(Long nid) {
        this.nid = nid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getAtime() {
        return atime;
    }

    public void setAtime(Date atime) {
        this.atime = atime;
    }
}
