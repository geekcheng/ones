package com.wsx.ones.custom;

import com.wsx.ones.web.controller.BaseController;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.BaseUser;
import com.wsx.ones.web.util.OperationEnum;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangshuaixin on 16/12/18.
 */
@RestController
@RequestMapping("/Custom")
public class CustomBaseController extends BaseController implements WebController {

    /**
     *
     * @param customParam
     * @param operationEnum
     * @return
     */
    protected boolean checkCenterParam(CustomParam customParam, OperationEnum operationEnum) {

        return true;
    }

    @Override
    public BaseUser setUserCache(BaseUser baseUser, boolean ifEhcache) {
        return null;
    }

    @Override
    public BaseUser getUserCache(String uid, boolean ifEhcache) {
        return null;
    }
}
