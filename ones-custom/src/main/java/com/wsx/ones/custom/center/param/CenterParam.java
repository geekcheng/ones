package com.wsx.ones.custom.center.param;

import com.wsx.ones.custom.CustomParam;

/**
 * Created by wangshuaixin on 17/1/4.
 */
public class CenterParam extends CustomParam {

    private static final long serialVersionUID = 4852969934322130982L;

    private Long nid;
    private String name;

    public Long getNid() {
        return nid;
    }

    public void setNid(Long nid) {
        this.nid = nid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
