package com.wsx.ones.zone.category.service.impl;

import com.google.common.base.Strings;
import com.wsx.ones.web.bo.PageBo;
import com.wsx.ones.web.swap.ReturnStatus;
import com.wsx.ones.web.util.PojoCopy;
import com.wsx.ones.web.vo.PageVo;
import com.wsx.ones.zone.ZoneVo;
import com.wsx.ones.zone.category.bo.CateSearchBo;
import com.wsx.ones.zone.category.bo.CategoryBo;
import com.wsx.ones.zone.category.dao.CategoryDao;
import com.wsx.ones.zone.category.model.CateSearch;
import com.wsx.ones.zone.category.model.mongo.ZoneCate;
import com.wsx.ones.zone.category.service.CategoryService;
import com.wsx.ones.zone.util.MongoDataUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by wangshuaixin on 16/12/29.
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryDao categoryDao;

    /**
     *
     * @param categoryBo
     * @return
     */
    public ZoneVo saveCate(CategoryBo categoryBo) {

        ZoneCate zoneCate = PojoCopy.copy(categoryBo, ZoneCate.class);

        categoryDao.saveCate(zoneCate);
        if (Strings.isNullOrEmpty(zoneCate.get_id())) {
            return new ZoneVo(ReturnStatus.ERROR_UNKNOW);
        }

        ZoneVo zoneVo = new ZoneVo();
        zoneVo.set_id(zoneCate.get_id());
        return zoneVo;
    }

    /**
     *
     * @param pageBo
     * @return
     */
    public PageVo getAllCates(PageBo pageBo) {

        List<Map> cates = categoryDao.getAllCates(pageBo.getPageNo(), pageBo.getPageSize());

        if (null == cates) {
            return new PageVo(ReturnStatus.ERROR_UNKNOW);
        }

        if (cates.size() <= 0) {
            return new PageVo(ReturnStatus.NOT_EXIST);
        }

        List<Map<String, String>> rtns = new ArrayList<Map<String, String>>();
        for (int i = 0; i < cates.size(); i++) {
            rtns.add(MongoDataUtil.transFromObject(cates.get(i)));
        }

        PageVo pageVo = new PageVo();
        pageVo.setResults(rtns);
        return pageVo;
    }

    /**
     *
     * @param bo
     * @return
     */
    public PageVo getSearchCates(CateSearchBo bo) {

        CateSearch search = PojoCopy.copy(bo, CateSearch.class);

        List<Map> cates = categoryDao.getSearchCates(search);
        if (null == cates) {
            return new PageVo(ReturnStatus.ERROR_UNKNOW);
        }

        if (cates.size() <= 0) {
            return new PageVo(ReturnStatus.NOT_EXIST);
        }

        List<Map<String, String>> rtns = new ArrayList<Map<String, String>>();
        for (int i = 0; i < cates.size(); i++) {
            rtns.add(MongoDataUtil.transFromObject(cates.get(i)));
        }

        return new PageVo(ReturnStatus.SUCCESS, rtns);
    }
}
