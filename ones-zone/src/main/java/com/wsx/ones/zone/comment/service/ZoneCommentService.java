package com.wsx.ones.zone.comment.service;

import com.wsx.ones.zone.comment.model.mongo.ZoneComment;

/**
 * Created by wangshuaixin on 16/12/11.
 */
public interface ZoneCommentService {

    boolean saveComment(ZoneComment zoneComment);
}
