package com.wsx.ones.zone.comment.service.impl;

import com.wsx.ones.zone.comment.dao.ZoneCommentDao;
import com.wsx.ones.zone.comment.model.mongo.ZoneComment;
import com.wsx.ones.zone.comment.service.ZoneCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wangshuaixin on 16/12/11.
 */
@Service
@Transactional
public class ZoneCommentServiceImpl implements ZoneCommentService {

    @Autowired
    private ZoneCommentDao zoneCommentDao;

    public boolean saveComment(ZoneComment zoneComment) {
        zoneCommentDao.saveComment(zoneComment);
        return true;
    }
}
