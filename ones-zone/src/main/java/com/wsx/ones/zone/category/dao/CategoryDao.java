package com.wsx.ones.zone.category.dao;

import com.wsx.ones.zone.category.model.CateSearch;
import com.wsx.ones.zone.category.model.mongo.ZoneCate;

import java.util.List;
import java.util.Map;

/**
 * Created by wangshuaixin on 16/12/29.
 */
public interface CategoryDao {

    void saveCate(ZoneCate zoneCate);

    List<Map> getAllCates(Integer pageNo, Integer pageSize);

    List<Map> getSearchCates(CateSearch search);
}
