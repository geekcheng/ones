package com.wsx.ones.zone.category.param;

import com.wsx.ones.web.model.PageParam;

import java.util.Date;

/**
 * Created by wangshuaixin on 16/12/30.
 */
public class CateSearchParam extends PageParam {

    private static final long serialVersionUID = 9037722675277975417L;

    private String name;
    private String atime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAtime() {
        return atime;
    }

    public void setAtime(String atime) {
        this.atime = atime;
    }
}
