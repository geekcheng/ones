package com.wsx.ones.zone.category.service;

import com.wsx.ones.web.bo.PageBo;
import com.wsx.ones.web.vo.PageVo;
import com.wsx.ones.zone.ZoneVo;
import com.wsx.ones.zone.category.bo.CateSearchBo;
import com.wsx.ones.zone.category.bo.CategoryBo;

/**
 * Created by wangshuaixin on 16/12/29.
 */
public interface CategoryService {

    ZoneVo saveCate(CategoryBo categoryBo);

    PageVo getAllCates(PageBo pageBo);

    PageVo getSearchCates(CateSearchBo bo);
}
