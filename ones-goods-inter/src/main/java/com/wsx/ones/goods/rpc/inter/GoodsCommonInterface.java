package com.wsx.ones.goods.rpc.inter;

import com.wsx.ones.goods.rpc.model.Goods;

public interface GoodsCommonInterface {

	public String get();


	public Goods take(String name);

}
